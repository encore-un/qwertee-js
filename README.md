# qwertee-js [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]
> NodeJS Qwertee API

## Installation

```sh
$ npm install --save qwertee-js
```

## Usage

```js
var qwerteeJs = require('qwertee-js');

qwerteeJs('Rainbow');
```
## License

BSD-2-Clause-FreeBSD © [Mania]()


[npm-image]: https://badge.fury.io/js/qwertee-js.svg
[npm-url]: https://npmjs.org/package/qwertee-js
[travis-image]: https://travis-ci.org//qwertee-js.svg?branch=master
[travis-url]: https://travis-ci.org//qwertee-js
[daviddm-image]: https://david-dm.org//qwertee-js.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//qwertee-js
[coveralls-image]: https://coveralls.io/repos//qwertee-js/badge.svg
[coveralls-url]: https://coveralls.io/r//qwertee-js
